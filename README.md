# MOVEBALL

Tennis game written on pure JavaScript with DOM preloading.

[PLAY DEMO!](http://malcolmmadsheep.com/moveball/)

**Author**: Zhuk Vladislav
<br />
**Email**: malcolm.madsheep@gmail.com
<br />
**Phone numb./Telegram**: +38(066)041-94-95